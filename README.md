# OpenML dataset: Meta_Album_BTS_Micro

https://www.openml.org/d/44279

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Boats Dataset (Micro)**
***
The original version of the Meta-Album boats dataset is called MARVEL dataset (https://github.com/avaapm/marveldataset2016). It has more than 138 000 images of 26 different maritime vessels in their natural background. Each class can have 1 802 to 8 930 images of variable resolutions. To preprocess this dataset, we either duplicate the top and bottom-most 3 rows or the left and right most 3 columns based on the orientation of the original image to create square images. No cropping was applied because the boats occupy most of the image, and applying this technique will lead to incomplete images. Finally, the square images were resized into 128x128 px using an anti-aliasing filter  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/BTS.png)

**Meta Album ID**: VCL.BTS  
**Meta Album URL**: [https://meta-album.github.io/datasets/BTS.html](https://meta-album.github.io/datasets/BTS.html)  
**Domain ID**: VCL  
**Domain Name**: Vehicles  
**Dataset ID**: BTS  
**Dataset Name**: Boats  
**Short Description**: Dataset with images of different boats  
**\# Classes**: 20  
**\# Images**: 800  
**Keywords**: vehicles, boats  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Cite paper to use dataset  
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: MARVEL: A LARGE-SCALE IMAGE DATASET FOR MARITIME VESSELS  
**Source URL**: https://github.com/avaapm/marveldataset2016  
  
**Original Author**: Gundogdu E., Solmaz B, Yucesoy V., Koc A.  
**Original contact**:   

**Meta Album author**: Dustin Carrion  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@InProceedings{MARVEL,
    author="Gundogdu, Erhan and Solmaz, Berkan and Yucesoy, Veysel and Koc, Aykut",
    editor="Lai, Shang-Hong and Lepetit, Vincent and Nishino, Ko and Sato, Yoichi",
    title="MARVEL: A Large-Scale Image Dataset for Maritime Vessels",
    booktitle="Computer Vision --  ACCV 2016",
    year="2017",
    publisher="Springer International Publishing",
    address="Cham",
    pages="165--180",
    isbn="978-3-319-54193-8"
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Mini]](https://www.openml.org/d/44309)  [[Extended]](https://www.openml.org/d/44343)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44279) of an [OpenML dataset](https://www.openml.org/d/44279). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44279/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44279/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44279/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

